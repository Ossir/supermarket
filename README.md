# README #

Тестовое задание "Супермаркет".
В приложении реализована возможность просматривать, редактировать, удалять товары из локальной БД. Присутствуют фильтры 
по категориям товаров. Тестовая база заполняется один раз при первом запуске приложения. Товары, цены и категории можно
менять и добавлять в .txt файлах. Есть простая версия корзины. На данном этапе для нее нет отдельной таблицы. У товаров
имеется поле, определяющее, находится ли товар в корзине. Так же отсутсвует возможность указать количество едениц товара
в корзине.

### Как начать? ###

Склонировать репозиторий. Запустить в терминале pod install.
