//
//  Product+CoreDataProperties.h
//  
//
//  Created by Ossir on 23.10.17.
//
//

#import "Product+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Product (CoreDataProperties)

+ (NSFetchRequest<Product *> *)fetchRequest;

@property (nonatomic) BOOL byUser;
@property (nullable, nonatomic, copy) NSString *image;
@property (nullable, nonatomic, copy) NSDecimalNumber *price;
@property (nullable, nonatomic, copy) NSString *title;
@property (nonatomic) BOOL isInCart;
@property (nullable, nonatomic, retain) Category *categiryId;

@end

NS_ASSUME_NONNULL_END
