//
//  Product+CoreDataProperties.m
//  
//
//  Created by Ossir on 23.10.17.
//
//

#import "Product+CoreDataProperties.h"

@implementation Product (CoreDataProperties)

+ (NSFetchRequest<Product *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Product"];
}

@dynamic byUser;
@dynamic image;
@dynamic price;
@dynamic title;
@dynamic isInCart;
@dynamic categiryId;

@end
