//
//  Product+CoreDataClass.h
//  
//
//  Created by Ossir on 23.10.17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Category;

NS_ASSUME_NONNULL_BEGIN

@interface Product : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Product+CoreDataProperties.h"
