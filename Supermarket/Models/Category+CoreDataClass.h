//
//  Category+CoreDataClass.h
//  Supermarket
//
//  Created by Ossir on 20.10.17.
//  Copyright © 2017 ossir. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Product;

NS_ASSUME_NONNULL_BEGIN

@interface Category : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Category+CoreDataProperties.h"
