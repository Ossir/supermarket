//
//  AddProductViewController.m
//  Supermarket
//
//  Created by Ossir on 22.10.17.
//  Copyright © 2017 ossir. All rights reserved.
//

#import "AddProductViewController.h"
#import "DBHelper+Product.h"
#import "Category+CoreDataProperties.h"
#import <GKActionSheetPicker/GKActionSheetPicker.h>

@interface AddProductViewController () {
    Category *selectedCategory;
    NSString *imageURL;
}

@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet UITextField *productTitle;
@property (weak, nonatomic) IBOutlet UITextField *productPrice;
@property (weak, nonatomic) IBOutlet UILabel *productCategory;
@property (strong, nonatomic) Product *product;
@property (nonatomic, strong) GKActionSheetPicker *actionSheetPicker;

@end

@implementation AddProductViewController

- (IBAction)saveProductInfo:(id)sender {
    if (selectedCategory && imageURL) {
        [[DBHelper sharedInstance] saveOrUpdateProduct:self.product title:self.productTitle.text price:self.productPrice.text category:selectedCategory image:[[NSURL alloc] initWithString:imageURL] byUser:YES];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
        NSLog(@"");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.product) {
        imageURL = self.product.image;
        self.productImage.image = [self getImageByName:self.product.image];
        self.productTitle.text = self.product.title;
        self.productCategory.text = self.product.categiryId.title;
        self.productPrice.text = [self.product.price stringValue];
        selectedCategory = self.product.categiryId;
    }
    // Do any additional setup after loading the view.
}

- (NSArray *)fillPickerData {
    NSMutableArray *pickerData = [NSMutableArray new];
    NSArray *DBCategories = [[DBHelper sharedInstance] getAllObjectsWithEntityName:@"Category"];
    for (Category *category in DBCategories) {
        [pickerData addObject:[GKActionSheetPickerItem pickerItemWithTitle:category.title value:category]];
    }
    return [NSArray arrayWithArray:pickerData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)chooseCategory:(id)sender {
    self.actionSheetPicker = [GKActionSheetPicker stringPickerWithItems:[self fillPickerData] selectCallback:^(id selected) {
        selectedCategory = selected;
        self.productCategory.text = selectedCategory.title;
    } cancelCallback:nil];
    
    [self.actionSheetPicker presentPickerOnView:self.view];
}

- (IBAction)uploadImage:(id)sender {
    UIAlertController* alert = [UIAlertController
                                alertControllerWithTitle:nil
                                message:nil
                                preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction* cancelBtn = [UIAlertAction
                                actionWithTitle:@"Cancel"
                                style:UIAlertActionStyleCancel
                                handler:nil];
    UIAlertAction* photoLibBtn = [UIAlertAction
                                  actionWithTitle:@"Photo Library"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      [self openImagePicker:UIImagePickerControllerSourceTypePhotoLibrary];
                                  }];
    [alert addAction:cancelBtn];
    [alert addAction:photoLibBtn];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)openImagePicker:(UIImagePickerControllerSourceType)type {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.delegate = self;
    imagePickerController.sourceType = type;
    imagePickerController.allowsEditing = YES;
    if (type == UIImagePickerControllerSourceTypeCamera) {
        imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    }
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    [self.productImage setImage:image];
    imageURL = [NSString stringWithFormat:@"%@", [editingInfo valueForKey:UIImagePickerControllerImageURL]];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (UIImage *)getImageByName:(NSString *)name {
    NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString* filePath = [documentsPath stringByAppendingPathComponent:name];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    return [UIImage imageWithData:data];
}

@end
