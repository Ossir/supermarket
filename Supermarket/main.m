//
//  main.m
//  Supermarket
//
//  Created by Ossir on 20.10.17.
//  Copyright © 2017 ossir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
