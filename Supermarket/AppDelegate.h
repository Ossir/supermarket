//
//  AppDelegate.h
//  Supermarket
//
//  Created by Ossir on 20.10.17.
//  Copyright © 2017 ossir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

