//
//  AddProductViewController.h
//  Supermarket
//
//  Created by Ossir on 22.10.17.
//  Copyright © 2017 ossir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product+CoreDataProperties.h"

@interface AddProductViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

- (void)setProduct:(Product *)product;

@end
