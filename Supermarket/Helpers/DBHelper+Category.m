//
//  DBHelper+Category.m
//  Supermarket
//
//  Created by Ossir on 20.10.17.
//  Copyright © 2017 ossir. All rights reserved.
//

#import "DBHelper+Category.h"
#import "Category+CoreDataProperties.h"

@implementation DBHelper (Category)

- (void)saveCategory:(NSString *)title {
    Category *category = [NSEntityDescription insertNewObjectForEntityForName:@"Category" inManagedObjectContext:[self getContext]];
    category.title = [title stringByTrimmingLeadingWhitespace];
    [self saveContext];
}

@end
