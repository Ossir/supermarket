//
//  DBHelper+Category.h
//  Supermarket
//
//  Created by Ossir on 20.10.17.
//  Copyright © 2017 ossir. All rights reserved.
//

#import "DBHelper.h"

@interface DBHelper (Category)

- (void)saveCategory:(NSString *)title;

@end
