//
//  DBHelper.m
//  Supermarket
//
//  Created by Ossir on 20.10.17.
//  Copyright © 2017 ossir. All rights reserved.
//

#import "DBHelper.h"
#import "DBHelper+Category.h"
#import "DBHelper+Product.h"

@interface DBHelper ()

@property (readonly, strong) NSPersistentContainer *persistentContainer;

@end

@implementation DBHelper

- (int)getRand:(long)upperBound {
    return arc4random() % upperBound;
}

- (void)fillTestDB {
    NSArray *categories = [self loadFileContent:@"Categories"];
    for (NSString* category in categories) {
        [self saveCategory:category];
    }
    NSArray *DBCategories = [self getAllObjectsWithEntityName:@"Category"];
    NSArray *productTitles = [self loadFileContent:@"ProductTitles"];
    NSArray *prices = [self loadFileContent:@"Prices"];
    
    NSURL *bundleRoot = [[NSBundle mainBundle] bundleURL];
    NSArray * dirContents =
    [[NSFileManager defaultManager] contentsOfDirectoryAtURL:bundleRoot
      includingPropertiesForKeys:@[]
                         options:NSDirectoryEnumerationSkipsHiddenFiles
                           error:nil];
    NSArray *extensions = [NSArray arrayWithObjects:@"png", @"jpg", nil];
    NSArray *images = [dirContents filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"pathExtension IN %@", extensions]];

    for (int i = 0; i < 300; i++) {
        [self saveOrUpdateProduct:nil title:productTitles[[self getRand:[productTitles count]]] price:prices[[self getRand:[prices count]]] category:DBCategories[[self getRand:[DBCategories count]]] image:images[[self getRand:[images count]]] byUser:NO];
    }
}

- (NSArray *)loadFileContent:(NSString *)name {
    NSString* path = [[NSBundle mainBundle] pathForResource:name
                                                     ofType:@"txt"];
    NSString* content = [NSString stringWithContentsOfFile:path
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    return [content componentsSeparatedByString:@","];
}

- (NSString *)documentsPathForFileName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    
    return [documentsPath stringByAppendingPathComponent:name];
}

+ (DBHelper *)sharedInstance {
    static DBHelper *_dbHelper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _dbHelper = [[self alloc] init];
    });
    
    return _dbHelper;
}

- (NSArray *)getAllObjectsWithEntityName:(NSString *)entityName {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]initWithEntityName:entityName];
    NSError *requestError = nil;
    if (requestError)
    {
        NSLog(@"DB error %@", requestError.localizedDescription);
        return [NSArray new];
    }
    else
        return [[self getContext] executeFetchRequest:fetchRequest error:&requestError];
}

- (NSManagedObjectContext *)getContext {
    return self.persistentContainer.viewContext;
}

#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"Supermarket"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
