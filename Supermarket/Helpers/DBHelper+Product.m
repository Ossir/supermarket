//
//  DBHelper+Product.m
//  Supermarket
//
//  Created by Ossir on 20.10.17.
//  Copyright © 2017 ossir. All rights reserved.
//

#import "DBHelper+Product.h"
#import "Product+CoreDataProperties.h"

@implementation DBHelper (Product)

- (void)saveOrUpdateProduct:(Product*)product title:(NSString *)title price:(NSString *)price category:(Category *)category image:(NSURL *)image byUser:(BOOL)byUser{
    Product *newProduct;
    if (!product) {
        newProduct = [NSEntityDescription insertNewObjectForEntityForName:@"Product" inManagedObjectContext:[self getContext]];
    }
    else
        newProduct = product;
    newProduct.title = [title stringByTrimmingLeadingWhitespace];
    newProduct.price = [[NSDecimalNumber alloc] initWithString:price];
    newProduct.categiryId = category;
    newProduct.byUser = byUser;
    if (newProduct.image != [image absoluteString]) {
        NSURL *url = image;
        NSData *data = [NSData dataWithContentsOfURL:url];
        NSString *filePath = [self documentsPathForFileName:[image lastPathComponent]];
        [data writeToFile:filePath atomically:YES];
        newProduct.image = [image lastPathComponent];
    }
    [self saveContext];
}

- (void)deleteProduct:(Product *)product {
    [[self getContext] deleteObject:product];
}

- (void)updateProduct {
    [self saveContext];
}

@end
