//
//  DBHelper+Product.h
//  Supermarket
//
//  Created by Ossir on 20.10.17.
//  Copyright © 2017 ossir. All rights reserved.
//

#import "DBHelper.h"
#import "Product+CoreDataProperties.h"

@interface DBHelper (Product)

- (void)saveOrUpdateProduct:(Product*)product title:(NSString *)title price:(NSString *)price category:(Category *)category image:(NSURL *)image byUser:(BOOL)byUser;

- (void)deleteProduct:(Product *)product;

- (void)updateProduct;

@end
