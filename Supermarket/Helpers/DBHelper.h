//
//  DBHelper.h
//  Supermarket
//
//  Created by Ossir on 20.10.17.
//  Copyright © 2017 ossir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "NSString+Trimming.h"
#import <UIKit/UIKit.h>

@interface DBHelper : NSObject

+ (DBHelper *)sharedInstance;
- (void)saveContext;
- (NSArray *)getAllObjectsWithEntityName:(NSString *)entityName;
- (NSManagedObjectContext *)getContext;
- (void)fillTestDB;
- (NSString *)documentsPathForFileName:(NSString *)name;
@end
