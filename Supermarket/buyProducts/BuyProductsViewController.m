//
//  BuyProductsViewController.m
//  Supermarket
//
//  Created by Ossir on 20.10.17.
//  Copyright © 2017 ossir. All rights reserved.
//

#import "BuyProductsViewController.h"

@interface BuyProductsViewController ()

@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;

@end

@implementation BuyProductsViewController

@synthesize fetchedResultsController = _fetchedResultsController;

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    super.fetchedResultsController = _fetchedResultsController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    Category *selectedCategory = [self getCategory:0];
    _fetchedResultsController = [self fetchVC:@"Buy" predicate:[NSPredicate predicateWithFormat:@"categiryId.title = %@",selectedCategory.title]];
    _fetchedResultsController.delegate = self;
    return _fetchedResultsController;
}

- (void)dropdownMenu:(MKDropdownMenu *)dropdownMenu didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    Category *selectedCategory = [self getCategory:row];
    [NSFetchedResultsController deleteCacheWithName:@"Buy"];  
    [[_fetchedResultsController fetchRequest] setPredicate:[NSPredicate predicateWithFormat:@"categiryId.title = %@",selectedCategory.title]];
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:NO];
    [[_fetchedResultsController fetchRequest] setSortDescriptors:[NSArray arrayWithObject:sort]];
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        // Handle you error here
    }
    [self.tableView reloadData];
    [self setCategory:selectedCategory];
    [self.filterView reloadComponent:component];
}

@end
