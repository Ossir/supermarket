//
//  ProductTableViewCell.h
//  Supermarket
//
//  Created by Ossir on 20.10.17.
//  Copyright © 2017 ossir. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductTableViewCell : UITableViewCell

- (void)bindWithModel:(id)model;

@end
