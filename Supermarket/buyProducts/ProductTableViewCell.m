//
//  ProductTableViewCell.m
//  Supermarket
//
//  Created by Ossir on 20.10.17.
//  Copyright © 2017 ossir. All rights reserved.
//

#import "ProductTableViewCell.h"
#import "Product+CoreDataProperties.h"
#import "DBHelper+Product.h"

@interface ProductTableViewCell () {
    Product *product;
}

@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet UILabel *productTitle;
@property (weak, nonatomic) IBOutlet UILabel *productPrice;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;

@end

@implementation ProductTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (IBAction)addOrDeleteFromCart:(id)sender {
    product.isInCart = !product.isInCart;
    [[DBHelper sharedInstance] updateProduct];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)bindWithModel:(id)model {
    product = model;
    self.productPrice.text = [NSString stringWithFormat:@"%@Р",[product.price stringValue]];
    self.productTitle.text = product.title;
    self.productImage.image = [self getImageByName:product.image];
    if (product.isInCart)
        [self.buyButton setTitle:@"Remove" forState:UIControlStateNormal];
    else
        [self.buyButton setTitle:@"Buy" forState:UIControlStateNormal];
}

- (UIImage *)getImageByName:(NSString *)name {
    NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString* filePath = [documentsPath stringByAppendingPathComponent:name];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    return [UIImage imageWithData:data];
}

@end
