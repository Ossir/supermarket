//
//  NSString+Trimming.h
//  Supermarket
//
//  Created by Ossir on 22.10.17.
//  Copyright © 2017 ossir. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Trimming)

-(NSString *) stringByTrimmingLeadingWhitespace;

@end
