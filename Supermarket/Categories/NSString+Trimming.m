//
//  NSString+Trimming.m
//  Supermarket
//
//  Created by Ossir on 22.10.17.
//  Copyright © 2017 ossir. All rights reserved.
//

#import "NSString+Trimming.h"

@implementation NSString (Trimming)

-(NSString *) stringByTrimmingLeadingWhitespace
{
    NSInteger i = 0;
    
    NSString *str = [self stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    while ((i < [str length])
           && [[NSCharacterSet whitespaceCharacterSet] characterIsMember:[str characterAtIndex:i]]) {
        i++;
    }
    return [str substringFromIndex:i];
}

@end
