//
//  BaseViewController.h
//  Supermarket
//
//  Created by Ossir on 22.10.17.
//  Copyright © 2017 ossir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBHelper.h"
#import <MKDropdownMenu/MKDropdownMenu.h>
#import "Category+CoreDataProperties.h"

@interface BaseViewController : UIViewController <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate, MKDropdownMenuDataSource, MKDropdownMenuDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet MKDropdownMenu *filterView;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;

- (Category*)getCategory:(NSInteger)row;
- (void)setCategory:(Category*)category;
- (NSFetchedResultsController *)fetchVC:(NSString *)cacheName predicate:(NSPredicate*)predicate;
@end
